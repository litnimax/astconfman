��                               &   ,  &   S  .   z     �     �  Z   �     $     *     1     :     ?     H     W     \     e  +   |     �      �     �     �  
                       ,     2     B     Q  
   X  �  c  D     P   X  V   �  C      *   D     o  �   �  
   :     E     Z     l     }  )   �     �     �  *   �  =   	  A   ]	  ?   �	     �	     �	     
     
  &   *
  $   Q
     v
     �
  !   �
     �
     �
   Channel %(channel)s is kicked. Conference %(room)s recording started. Conference %(room)s recording stopped. Conference for room %(room)s has been started. Conference is over. Conferences Could not verify your access level for that URL.
You have to login with proper credentials Count Invite Is admin Kick Kick All Login Required Mute Mute All No such room: %(room)s Number %(number)s is called for conference. Participant %(channel)s muted. Participant %(channel)s unmuted. Participants Phone number Recordings Room Room muted. Room unmuted. Rooms Start Recording Stop Recording Unmute Unmute All Project-Id-Version: PROJECT VERSION
Report-Msgid-Bugs-To: EMAIL@ADDRESS
POT-Creation-Date: 2015-08-02 21:47+0300
PO-Revision-Date: 2015-08-02 21:49+0300
Last-Translator: 
Language-Team: ru <LL@li.org>
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2)
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
Generated-By: Babel 2.0
 Канал %(channel)s выброшен из конференции. Запись конференции в комнате %(room)s началась. Запись конференции в комнате %(room)s закончилась. Конференция в комнате %(room)s началась. Конференция завершена. Конференции Не удалось подтвердить права доступа к данному URL.
Введите логин и пароль для авторизации доступа. Число Пригласить Это админ Выкинуть Выкинуть всех Требуется авторизация Приглушить Приглушить всех Нет такой комнаты: %(room)s Номер %(number)s вызван в конференцию. Участник %(channel)s - микрофон выключен. Участник %(channel)s - микрофон включен. Участники Номер телефона Записи Комната Микрофоны выключены. Микрофоны включены. Комнаты Начать запись Остановить запись Влючить Включить всех 